/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session6;

/**
 *
 * @author Docente
 */
public class Depreciacion {
    
    public static double lineaRecta(double va, double vs, int vu){
        double lr = 0.0;
        if(vu == 0){
            return lr;
        }
        
        if((va - vs) == 0){
            return lr;
        }
        
        lr = ((double)(va - vs)/vu);
        
        return lr;
    }
    
    public static double[] sdaIncremental(double va, double vs, int vu){
        double sdai[] = null;
        
        if(vu == 0){
            return sdai;
        }
        
        if((va - vs ) == 0){
            return sdai;
        }
        
        int factor = 0;
        for(int i = 1; i <= vu; i++){
            factor += i; 
        }
        
        sdai = new double[vu];
        for(int i = 0; i < vu; i++){
            sdai[i] = (va - vs) * ((double)(i+1)/factor);
        }
        
        return sdai;
    }
}
