/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package componentes.jtable.renderer;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author JADPA21
 */
public class VehiculoTableModel extends AbstractTableModel {
    private List<Vehiculo> vehiculos;
    private List<String> columnNames;

    public VehiculoTableModel(List<Vehiculo> vehiculos, List<String> columnNames) {
        this.vehiculos = vehiculos;
        this.columnNames = columnNames;
    }    
    
    @Override
    public int getRowCount() {
        return vehiculos == null ? 0 : vehiculos.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames == null ? 0 : columnNames.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if(vehiculos.isEmpty()){
            return null;
        }
        if(rowIndex < 0 || rowIndex >= vehiculos.size()){
            return null;
        } 
        
        return vehiculos == null ? null : vehiculos.get(rowIndex);
    }
    
    @Override
    public String getColumnName(int column) {
        return columnNames.get(column);
    }
    
     public int addRow() {
        return addRow(new Vehiculo());
    }

    public int addRow(Vehiculo row) {
        vehiculos.add(row);
        fireTableRowsInserted(vehiculos.size() - 1, vehiculos.size() - 1);
        return vehiculos.size() - 1;
    }
    
}
