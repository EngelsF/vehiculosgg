/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversiones;

/**
 *
 * @author JADPA21
 */
public class TemperaturaModel {
    private float celcius;
    private float fharenheit;

    public TemperaturaModel() {
    }

    public float getCelcius() {
        return celcius;
    }

    public void setCelcius(float celcius) {
        this.celcius = celcius;
    }

    public float getFharenheit() {
        return fharenheit;
    }

    public void setFharenheit(float fharenheit) {
        this.fharenheit = fharenheit;
    }
    
    
}
